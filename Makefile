# makefile
#
PROG=pinplotter3
IMAGE=vai.ppm

# bluetooth ID and firmware version of target Mindstorms NXT
#
NXT=00:16:53:03:6b:53
NXT_FIRMWARE=101

# targets
#
all:: $(PROG).rxe

clean:
	rm -f $(PROG).rxe $(PROG).err $(PROG).nbc $(PROG).lst $(PROG).inf

$(PROG).rxe: $(PROG).nxc
	nbc -v=$(NXT_FIRMWARE) -sm- -Z2 -ER=1 -O=$(PROG).rxe $(PROG).nxc
	@ls -al

send:
	nxtrc -v -a $(NXT) -W $(PROG).rxe
	@sleep 1

sendimage:
	nxtrc -v -a $(NXT) -W $(IMAGE)

run:
	nxtrc -v -a $(NXT) -p $(PROG).rxe

