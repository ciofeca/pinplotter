## Description

Software for my "needle plotter" made out of LEGO bricks back in 2008. Prints a bitmap by punching holes on a sheet of paper.

## Hardware

"MOC": my own creation - LEGO machine based on [Mindstorms NXT](https://mindstorms.lego.com) intelligent brick (the original version released in 2006), motors, sensors, and other LEGO parts. The only non-LEGO part is the 0.3mm steel needle glued to an old LEGO brick. See [project images and schematics](https://www.brickshelf.com/cgi-bin/gallery.cgi?f=336687).

![Complete assembly](./31-top-view-before-start.jpg)

I demoed it in 2009 at Ballabio Italian LEGO Users Group ([ItLUG](https://itlug.org)) and other ItLUG meetings. I never attempted to create instructions to rebuild it. [Video showing it running](https://www.youtube.com/watch?v=NoUz5RTEjaY).

Note: Mindstorms NXT was discontinued and superseded by Mindstorms EV3 back in 2013.

## Specifications

- printable area: 91×70mm (3.6"x2.7")
- maximum resolution: 1.3 holes/mm (33 dpi), if using a 0.3mm needle
- time to print a bitmap of about 1,500 holes: about 35-45 minutes
- materials: only LEGO parts (one NXT brick, three NXT motors, three NXT touch-sensors, lots of Technic bricks) except the needle
- software: 14k of NXC commented source
- "printable" media: 105mm-wide paper sheets (best results using 80-100g/m² paper), plastic sheets (not too thick), kitchen "aluminium sheets"...
- NXT brick batteries: 6× 1.2V NiMH AA batteries (rated "2300mAh") allow 90 to 120 minutes printing

## Pre-requisites

NXT brick has to be Bluetooth paired to your PC.

Source is written in NXC ("Not Exactly C", a C-like language and library to compile RXE executables for the LEGO Mindstorms NXT) using [BricxCC](http://bricxcc.sourceforge.net/) - BricxCC can be installed from Ubuntu using: _sudo apt install nbc_

Sending RXE executables and PPM files to the NXT brick via Bluetooth: use the [nxtrc utility](http://www.scienzaludica.it/index0e9b.html?page=88) utility.

Everything was tested against NXT firmware releases 1.01 and 1.05.

Don't forget to update the NXT Bluetooth address in the *Makefile* if you use *make send* or *make sendimage*.

## Workflow

1) Have the *pinplotter* source compiled to RXE and installed on the NXT; for example: *make && make send*

2) Use an image editor to create a 116x91, black/white (1 bit per pixel) image; zooming and grid will help. Example: *xpaint -fullmenu -canvas -zoom 10 -size 116x91*

3) Draw "black" pixels where the holes shall be punched. Export the image to *PPM* "1 bit per pixel" format ("*P4*" mode, 116x91, always 1375 bytes)

4) Send the PPM file to the NXT; for example: *cp myimage.ppm vai.ppm; IMAGE=vai.ppm make sendimage*

5) Start the *pinplotter* RXE program on the NXT.
